﻿create or replace function random_string(length integer) returns text as 
$$
declare
  chars text[] := '{0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';
  result text := '';
  i integer := 0;
begin
  if length < 0 then
    raise exception 'Given length cannot be less than 0';
  end if;
  for i in 1..length loop
    result := result || chars[1+random()*(array_length(chars, 1)-1)];
  end loop;
  return result;
end;
$$ language plpgsql;

INSERT INTO ProductCategory(ProductCategoryID, Name)
WITH RECURSIVE IDS(ID) AS
(VALUES(4) UNION ALL
SELECT ID+1 FROM IDS
WHERE ID < 14)
SELECT ID,
md5(random()::text)
FROM IDS;

INSERT INTO ProductSubcategory(ProductSubcategoryID, ProductCategoryID, Name)
WITH RECURSIVE IDS(ID) AS
(VALUES(7) UNION ALL
SELECT ID + 1 FROM IDS
WHERE ID <40)
SELECT ID,
CAST(CEIL((random()+0.01) * (SELECT MAX(ProductCategoryID)-1 FROM ProductCategory)) AS BIGINT),
md5(random()::text)
FROM IDS;

INSERT INTO Product(ProductID, Name, ProductNumber, Color, ProductSubcategoryID, SellStartDate)
WITH RECURSIVE IDS(ID) AS
(VALUES(6) UNION ALL
SELECT ID + 1 FROM IDS
WHERE ID < 100000)
SELECT ID,
random_string(20),
random_string(20),
CAST(CEIL((random()+0.01) * (SELECT MAX(ColorID)-1 FROM Color)) AS INTEGER),
CAST(CEIL((random()+0.01) * (SELECT MAX(ProductSubcategoryID)-1 FROM ProductSubcategory)) AS BIGINT),
NOW() - '1 day'::INTERVAL * ROUND(RANDOM()*150)
FROM IDS;

ALTER TABLE Document DROP CONSTRAINT chkdate;

INSERT INTO Document(DocumentID, Title, "Owner", FileName, FileExtension, DocumentSummary, CreatedDate, ModifiedDate)
WITH RECURSIVE IDS(ID) AS
(VALUES (4) UNION ALL
SELECT ID + 1 FROM IDS
WHERE ID < 150000)
SELECT ID,
random_string(20),
random_string(20),
random_string(20),
'docx',
random_string(10),
NOW() - '1 day'::INTERVAL * ROUND(RANDOM()*300),
NOW() - '1 day'::INTERVAL * ROUND(RANDOM()*200)
FROM IDS;

INSERT INTO mtm_Product_Document(ProductID, DocumentID)
WITH tuple(PID,DID) AS
(
SELECT 
CAST(CEIL((random()+0.01) * (SELECT MAX(ProductID)-1 FROM Product)) AS BIGINT),
CAST(CEIL((random()+0.01) * (SELECT MAX(DocumentID)-1 FROM Document)) AS BIGINT)
FROM generate_series(1, 160000)
)
SELECT DISTINCT PID,
	DID
FROM tuple
WHERE NOT EXISTS (
	SELECT * FROM mtm_Product_Document mpd 
	WHERE mpd.ProductID=PID 
	  AND mpd.DocumentID = DID)
      AND EXISTS (
        SELECT ProductID FROM Product p
        WHERE p.ProductID = PID)
      AND EXISTS (
	SELECT DocumentID FROM Document d
	WHERE d.DocumentID = DID);