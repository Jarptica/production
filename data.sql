﻿INSERT INTO ProductCategory(ProductCategoryID, Name)
VALUES (1, 'Крепления');
INSERT INTO ProductCategory(ProductCategoryID, Name)
VALUES (2, 'Краски');
INSERT INTO ProductCategory(ProductCategoryID, Name)
VALUES (3, 'Малярный инструмент');

INSERT INTO ProductSubcategory(ProductSubcategoryID, ProductCategoryID, Name)
VALUES (1, 1, 'Железные крепления');
INSERT INTO ProductSubcategory(ProductSubcategoryID, ProductCategoryID, Name)
VALUES (2, 1, 'Пластиковые крепления');
INSERT INTO ProductSubcategory(ProductSubcategoryID, ProductCategoryID, Name)
VALUES (3, 2, 'Акриловые краски');
INSERT INTO ProductSubcategory(ProductSubcategoryID, ProductCategoryID, Name)
VALUES (4, 2, 'Маслянные краски');
INSERT INTO ProductSubcategory(ProductSubcategoryID, ProductCategoryID, Name)
VALUES (5, 2, 'Расстворители');
INSERT INTO ProductSubcategory(ProductSubcategoryID, ProductCategoryID, Name)
VALUES (6, 3, 'Кисти');

INSERT INTO Color(ColorID, Name)
VALUES (1, 'Белый');
INSERT INTO Color(ColorID, Name)
VALUES (2, 'Черный');
INSERT INTO Color(ColorID, Name)
VALUES (3, 'Красный');
INSERT INTO Color(ColorID, Name)
VALUES (4, 'Зеленый');
INSERT INTO Color(ColorID, Name)
VALUES (5, 'Синий');

INSERT INTO Product(ProductID, Name, ProductNumber, Color, ProductSubcategoryID, SellStartDate)
VALUES (1, 'Гвозди', '1.0.42.B', 1, 1, '21/11/2012');
INSERT INTO Product(ProductID, Name, ProductNumber, Color, ProductSubcategoryID, SellStartDate, SellEndDate)
VALUES (2, 'Кисточка средняя', '28.4.F.', 2, 6, '14/10/2012', '12/02/2013');

INSERT INTO City(CityID, Name)
VALUES (1, 'Мариуполь');
INSERT INTO City(CityID, Name)
VALUES (2, 'Макленбург');
INSERT INTO City(CityID, Name)
VALUES (3, 'Нуук');
INSERT INTO City(CityID, Name)
VALUES (4, 'Квебек');
INSERT INTO City(CityID, Name)
VALUES (5, 'Шеньян');

INSERT INTO Country(CountryID, Name)
VALUES (1, 'Украина');
INSERT INTO Country(CountryID, Name)
VALUES (2, 'Польша');
INSERT INTO Country(CountryID, Name)
VALUES (3, 'Дания');
INSERT INTO Country(CountryID, Name)
VALUES (4, 'Канада');
INSERT INTO Country(CountryID, Name)
VALUES (5, 'Китай');

INSERT INTO Region(RegionID, Name)
VALUES (1, 'Донецкая обл');
INSERT INTO Region(RegionID, Name)
VALUES (2, 'Померания');
INSERT INTO Region(RegionID, Name)
VALUES (3, 'Зеландия');
INSERT INTO Region(RegionID, Name)
VALUES (4, 'Онтарио');
INSERT INTO Region(RegionID, Name)
VALUES (5, 'Юнань');

INSERT INTO Location(LocationID, Name, Country, Region, City, Street, HouseNumber)
VALUES (1, 'Азовмаш', 1, 1, 1, 'Машиностроителей', 1);
INSERT INTO Location(LocationID, Name, Country, Region, City, Street, HouseNumber)
VALUES (2, 'New Millenium', 2, 2, 2, 'Калишская', 15);

INSERT INTO Document(DocumentID, Title, "Owner", FileName, FileExtension, DocumentSummary, createddate, modifieddate)
VALUES (1, 'Описание МК-48', 'Иванов И.О.', 'МК-48132', 'doc', 'Инструкция', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO Document(DocumentID, Title, "Owner", FileName, FileExtension, DocumentSummary, createddate, modifieddate)
VALUES (2, 'Чертеж MK-33', 'Петрова М.Д.', 'МДК-03234324', 'cad', 'Чертеж крепления МВ-95', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO Document(DocumentID, Title, "Owner", FileName, FileExtension, DocumentSummary, createddate, modifieddate)
VALUES (3, 'Чертеж КВ-95', 'Чернов Е.К.', 'БМ-6282342', 'cad', 'Чертеж крепления ПР-06', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO BillOfMaterials(BillOfMaterialsID, ProductAssemblyID, ComponentID, PerAssenblyQty)
VALUES (1, 1, 1, 23);

INSERT INTO ProductPriceHistory(ProductID, Price, StartDate, EndDate)
VALUES (1, 0.33, '23/12/12', NULL);
INSERT INTO ProductPriceHistory(ProductID, Price, StartDate, EndDate)
VALUES (2, 2.21, '26/03/11', NULL);

INSERT INTO ProductInventory(ProductID, LocationID, Shelf, Bin, Quantity)
VALUES (1, 2, 46, 3, 1700);
INSERT INTO ProductInventory(ProductID, LocationID, Shelf, Bin, Quantity)
VALUES (2, 1, 92, 55, 1000);

INSERT INTO mtm_Product_Document(ProductID, DocumentID)
VALUES (1, 1);
INSERT INTO mtm_Product_Document(ProductID, DocumentID)
VALUES (2, 3);
INSERT INTO mtm_Product_Document(ProductID, DocumentID)
VALUES (2, 2);

INSERT INTO ProductCostHistory(ProductID, StartDate, EndDate, StandartCost, ModifiedDate)
VALUES (1, '22/04/11', NULL, 0.32, '22/08/11');
INSERT INTO ProductCostHistory(ProductID, StartDate, EndDate, StandartCost, ModifiedDate)
VALUES (2, '22/04/11', NULL, 2.01, '22/08/11');











