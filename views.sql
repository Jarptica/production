﻿CREATE VIEW ActualProductPrice
AS
	SELECT p.ProductID,
		p.Name as ProductName,
		p.ProductNumber,
		c.Name as Color,
		ps.Name as ProductSubcategoryName,
		p.SellStartDate,
		p.SellEndDate,
		pph.Price
	FROM (SELECT p.ProductID, MAX(pc.StartDate) as StartDate
	      FROM Product p
	      INNER JOIN ProductPriceHistory pc
	      ON p.ProductID = pc.ProductID
	      WHERE pc.StartDate < CURRENT_TIMESTAMP
	      GROUP BY p.ProductID) as sq
	INNER JOIN Product p
		ON sq.ProductID = p.ProductID
	INNER JOIN ProductPriceHistory pph
		ON p.ProductID=pph.ProductID
			AND sq.StartDate = pph.StartDate
	INNER JOIN Color c
		ON p.Color = c.ColorID
	INNER JOIN ProductSubcategory ps
		ON p.ProductSubcategoryID = ps.ProductSubcategoryID
	WHERE p.SellStartDate < CURRENT_TIMESTAMP
		AND (p.SellEndDate >= CURRENT_TIMESTAMP
		  OR p.SellEndDate IS NULL) ;

GRANT SELECT ON ActualProductPrice TO technicalwriter;

CREATE RULE ActualProductPrice_Insert
AS
ON INSERT TO ActualProductPrice DO INSTEAD 
(
	INSERT INTO Product(ProductID, Name, ProductNumber, Color, ProductSubcategoryID, SellStartDate, SellEndDate)
	VALUES (NEW.ProductID, NEW.ProductName, NEW.ProductNumber, 
		(SELECT c.ColorID 
		 FROM Color c
		 WHERE c.Name=NEW.Color),
		(SELECT ps.ProductSubcategoryID
		 FROM ProductSubcategory ps
		 WHERE ps.Name = NEW.ProductSubcategoryName),
		NEW.SellStartDate,
		NEW.SellEndDate);

	INSERT INTO ProductPriceHistory(ProductID, Price, StartDate)
	VALUES(NEW.ProductID, NEW.Price, CURRENT_TIMESTAMP);
);

CREATE OR REPLACE FUNCTION usp_nodelete()
RETURNS TRIGGER
AS $$
BEGIN
	ROLLBACK;
	RAISE EXCEPTION 'Delete not allowed';
END; $$
LANGUAGE plpgsql;

CREATE TRIGGER tr_actualproductprice_nodelete
INSTEAD OF DELETE
ON ActualProductPrice
FOR EACH ROW
EXECUTE PROCEDURE usp_nodelete();

CREATE RULE ActualProductPrice_Update
AS
ON UPDATE TO ActualProductPrice DO INSTEAD
(
	UPDATE ProductPriceHistory
	SET EndDate=CURRENT_TIMESTAMP
	WHERE ProductID=NEW.ProductID
		AND EndDate IS NULL;
		
	INSERT INTO ProductPriceHistory(ProductID, Price, StartDate)
	VALUES(NEW.ProductID, NEW.Price, CURRENT_TIMESTAMP);
);