﻿vacuum analyze;

EXPLAIN ANALYZE SELECT *
FROM product p
INNER JOIN mtm_product_document pd
  ON p.productid = pd.productid
INNER JOIN document d
  ON pd.documentid = d.documentid
WHERE d.createddate > (now() - interval '2 week')::timestamp;

EXPLAIN ANALYZE SELECT *
FROM product p
INNER JOIN mtm_product_document pd
  ON p.productid = pd.productid
INNER JOIN document d
  ON pd.documentid = d.documentid
WHERE d.filename = '7DC9PMiXy23pde6399Hp';

CREATE INDEX idx_document_created ON document USING btree(createddate);

CREATE INDEX idx_document_filename ON document(filename);

vacuum analyze;

EXPLAIN ANALYZE SELECT *
FROM product p
INNER JOIN mtm_product_document pd
  ON p.productid = pd.productid
INNER JOIN document d
  ON pd.documentid = d.documentid
WHERE d.createddate > (now() - interval '2 week')::timestamp;

EXPLAIN ANALYZE SELECT *
FROM product p
INNER JOIN mtm_product_document pd
  ON p.productid = pd.productid
INNER JOIN document d
  ON pd.documentid = d.documentid
WHERE d.filename = '7DC9PMiXy23pde6399Hp';