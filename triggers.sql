﻿ALTER TABLE Product
DROP CONSTRAINT IF EXISTS product_productsubcategoryid_fkey;

CREATE OR REPLACE FUNCTION usp_check_product_subcategory_fkey()
RETURNS TRIGGER
AS $$
BEGIN
	IF NOT EXISTS(
		SELECT ps.ProductSubcategoryID 
		FROM ProductSubcategory ps
		WHERE ps.ProductSubcategoryID=NEW.ProductSubcategoryID)
	 THEN RAISE EXCEPTION 'Foreign key reference violated, subcategory with subcategoryid % does not exist', NEW.ProductSubcategoryID;
	 END IF;

	RETURN NEW;
END; $$ 
LANGUAGE plpgsql;

CREATE TRIGGER trchk_product_subcategory_fkey
BEFORE INSERT OR UPDATE
ON Product
FOR EACH ROW
EXECUTE PROCEDURE usp_check_product_subcategory_fkey();

ALTER TABLE Product
ADD COLUMN SubcategoryName TEXT;

--fail data sample
--
-- INSERT INTO Product(ProductID, Name, ProductNumber, Color, ProductSubcategoryID, SellStartDate, SellEndDate)
-- VALUES (2, 'Кисточка средняя', '28.4.F.', NULL, 100500, '14/10/2012', '12/02/2013');

CREATE OR REPLACE FUNCTION usp_product_copy_subcategory_name()
RETURNS TRIGGER
AS $$
BEGIN
	UPDATE Product
	SET SubcategoryName = 
		(SELECT ps.Name 
		 FROM ProductSubcategory ps 
		 WHERE ps.ProductSubcategoryID = NEW.ProductSubcategoryID)
	WHERE ProductID = NEW.ProductID;
	RETURN NEW;
END; $$
LANGUAGE plpgsql;

CREATE TRIGGER tr_cpy_subcat_name
AFTER INSERT OR UPDATE OF ProductSubcategoryID
ON Product
FOR EACH ROW
EXECUTE PROCEDURE usp_product_copy_subcategory_name();


--copy trigger execute sample
INSERT INTO Product(ProductID, Name, ProductNumber, Color, ProductSubcategoryID, SellStartDate, SellEndDate)
VALUES (5, 'Кисточка 0.5м', '28.4.F.XXL', 1, 3, '14/10/2012', '12/02/2013');

SELECT * FROM Product;

CREATE OR REPLACE FUNCTION usp_log_product_price()
RETURNS TRIGGER
AS $$
BEGIN
	INSERT INTO Audit_Log(EntityName, EntityID, EntityAction, LogDate, UserLogin)
	VALUES(TG_RELNAME, COALESCE(NEW.ProductID, OLD.ProductID), TG_OP, CURRENT_TIMESTAMP, CURRENT_USER);

	RETURN NEW;
END; $$ 
LANGUAGE plpgsql;

CREATE TRIGGER tr_auditlog_productprice
AFTER INSERT OR UPDATE
ON ProductPriceHistory
FOR EACH ROW
EXECUTE PROCEDURE usp_log_product_price();

INSERT INTO ProductPriceHistory(ProductID, Price, StartDate)
VALUES(1, 1000, CURRENT_TIMESTAMP);

SELECT * FROM Audit_Log;

ALTER TABLE Product 
	DROP CONSTRAINT chkDates;

CREATE OR REPLACE FUNCTION usp_chk_product_dates()
RETURNS TRIGGER
AS $$
BEGIN
	IF(NEW.SellEndDate IS NOT NULL AND NEW.SellEndDate < NEW.SellStartDate)
		THEN RAISE EXCEPTION 'SellEndDate must be later then SellStartDate, SellStartDate: %', NEW.SellStartDate;
	END IF;
	RETURN NEW;
END; $$ 
LANGUAGE plpgsql;

CREATE TRIGGER trchk_product_dates
AFTER INSERT OR UPDATE
ON Product
FOR EACH ROW
EXECUTE PROCEDURE usp_chk_product_dates();

UPDATE Product
SET SellEndDate = CURRENT_TIMESTAMP - interval '5 years'
WHERE ProductID = 1;