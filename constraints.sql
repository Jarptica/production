﻿ALTER TABLE Document
	ADD CONSTRAINT chkFileName_Len CHECK (CHAR_LENGTH(FileName) > 5);

ALTER TABLE Document	
	ADD CONSTRAINT chkFileExtension_Len CHECK (CHAR_LENGTH(FileExtension) <= 5),
	ADD CONSTRAINT chkDate CHECK (ModifiedDate>=CreatedDate);

ALTER TABLE Product
	ADD CONSTRAINT chkName_Len CHECK (CHAR_LENGTH(Name) >= 4),
	ADD CONSTRAINT chkDates CHECK (SellEndDate > SellStartDate OR SellEndDate IS NULL);

ALTER TABLE Product
	DROP CONSTRAINT product_productsubcategoryid_fkey,
	ADD CONSTRAINT product_productsubcategoryid_fkey FOREIGN KEY (ProductSubcategoryID) 
		REFERENCES ProductSubcategory(ProductSubcategoryID) 
		ON DELETE CASCADE;

ALTER TABLE ProductSubcategory
	DROP CONSTRAINT productsubcategory_productcategoryid_fkey,
	ADD CONSTRAINT productsubcategory_productcategoryid_fkey FOREIGN KEY (ProductCategoryID) 
		REFERENCES ProductCategory(ProductCategoryID) 
		ON DELETE CASCADE; 

ALTER TABLE ProductCostHistory
	ADD CONSTRAINT chkPositiveCost CHECK (StandartCost > 0),
	ADD CONSTRAINT chkDates CHECK (EndDate > StartDate OR EndDate IS NULL);

ALTER TABLE ProductPriceHistory
	ADD CONSTRAINT chkPositivePrice CHECK (Price > 0);

ALTER TABLE BillOfMaterials
	ADD CONSTRAINT chkPositivePartCnt CHECK (PerAssenblyQty > 0);

ALTER TABLE ProductPriceHistory
	ADD CONSTRAINT chkDates CHECK (EndDate > StartDate OR EndDate IS NULL);