﻿UPDATE Product
SET Name='Гвозди'
WHERE ProductID=1;


SELECT *
FROM Product p
INNER JOIN ProductSubcategory ps
	ON p.ProductSubcategoryID = ps.ProductSubcategoryID
INNER JOIN ProductCategory pc
	ON ps.ProductCategoryID = pc.ProductCategoryID
LEFT JOIN ProductPriceHistory pph
	ON p.ProductID = pph.ProductID
LEFT JOIN ProductCostHistory pch
	ON p.ProductID = pch.ProductID

UNION ALL

SELECT *
FROM Product p
INNER JOIN ProductSubcategory ps
	ON p.ProductSubcategoryID = ps.ProductSubcategoryID
INNER JOIN ProductCategory pc
	ON ps.ProductCategoryID = pc.ProductCategoryID
LEFT JOIN ProductPriceHistory pph
	ON p.ProductID = pph.ProductID
LEFT JOIN ProductCostHistory pch
	ON p.ProductID = pch.ProductID;